function res = optFun1( x )
 load('data.mat');
    countData = length(global_1_r1);
    sum = 0;
    for i = 1 : countData
        sum = sum + log(logProb(global_1_r1(i,:), x, global_1_r1, i));
    end
    res = -1/countData*sum;
    
function res = logProb(point, h, data, iterator)
    countData = length(data);
    sum = 0;
    for i = 1:countData
        if(i ~= iterator)
            sum = sum + kernGaus(point(1),data(i,1),h(1))*...
                kernGaus(point(2),data(i,2),h(2));
        end
    end
    res = 1/((countData-1)*h(1)*h(2))*sum;
    
function res = parsanWindow(point1, point2, h)
    res = (point1 - point2)/h;
    if abs(res) < 0.5
        res = 1;
    else
        res = 0;
    end
    
function res = kernGaus(point1, point2, h)
    res = (point1 - point2)/h;
    res = 1/sqrt(2*pi)*exp(-res^2/2);