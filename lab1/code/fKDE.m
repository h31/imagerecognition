function pKDE = fKDE( N,h1,h2,x1,x2,X1,X2 )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
pKDE = 0;
for i=1:1:N
    if ( abs((x1 - X1(i))/h1) < 0.5 )
        K1 = 1;
    else
        K1 = 0;
    end
    if ( abs((x2 - X2(i))/h2) < 0.5 )
        K2 = 1;
    else
        K2 = 0;
    end
    pKDE = pKDE + K1*K2;
    
end
pKDE = pKDE /(N*h1*h2);
end

