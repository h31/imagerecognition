function lab1_3_1( mu_input, sigma_input, r )
hold on
axis([-2 5 -4 2]);

flags=['b', 'r', 'g'];
n=2; size=50;
mu{1}    = mu_input; 
sigma{1} = sigma_input;
%Численный расчет ММП
obj1 = gmdistribution.fit(r,1);
mu{2} = obj1.mu;
sigma{2} = obj1.Sigma;
%Аналитический расчет ММП
mu{3}=[0, 0];
sigma{3}=[0 0; 0 0];
for k = 1:1:size
	mu{3}=mu{3}+r(k,:);
end
mu{3}= mu{3}/size;
for k = 1:1:size
   sigma{3}=sigma{3}+(r(k,:)-mu{3})'*(r(k,:)-mu{3});
end
sigma{3}= sigma{3}/size;
%Построение эллипсов
for k = 1:1:3
    a=1/((2*pi)^(n/2)*sqrt(sigma{k}(1,1)*sigma{k}(2,2)));
    for i = 1:1:3
        c(i)=a*exp(-i^2/2);
    end
    x = linspace(mu{k}(1)-3*sqrt(sigma{k}(1,1)), mu{k}(1)+3*sqrt(sigma{k}(1,1)),size);
    y = linspace(mu{k}(2)-3*sqrt(sigma{k}(2,2)), mu{k}(2)+3*sqrt(sigma{k}(2,2)),size);
    [X1,X2] = meshgrid(x,y);
    Z      = mvnpdf([X1(:) X2(:)],mu{k},[sigma{k}(1,1) sigma{k}(2,2)]);
    Z      = reshape(Z,length(y),length(x));
    contour(X1,X2,Z,[c(1), c(2), c(3)], flags(k));
end
end

