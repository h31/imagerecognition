function res = pkde(mode,point,data, h1, h2)
    countPoint = length(data);
    summ = 0;
    summ2 = 0;
    switch(mode)
        case 1
            for i =1:countPoint
                summ = summ + parsanWindow(point(1),data(i,1),h1)*parsanWindow(point(2),data(i,2),h2);
            end
            res = 1/(countPoint*h1*h2)*summ;
        case 2
            for i =1:countPoint
                summ = summ + kernGaus(point(1),data(i,1),h1)*kernGaus(point(2),data(i,2),h2);
            end
            res = 1/(countPoint*h1*h2)*summ;
        case 3
            for i =1:countPoint
                summ = summ + kernGaus(point(1),data(i,1),h1);
                summ2 = summ2 + kernGaus(point(2),data(i,2),h2);
            end
            res = 1/(countPoint*h1*h2)*summ*summ2;
    end

function res = parsanWindow(point1, point2, h)
    res = (point1 - point2)/h;
    if abs(res) < 0.5
        res = 1;
    else
        res = 0;
    end
    
function res = kernGaus(point1, point2, h)
    res = (point1 - point2)/h;
    res = 1/sqrt(2*pi)*exp(-res^2/2);