function classIndex = SimpleMLCRand( data )
    class1 = 0;
    class2 = 0;
    class3 = 0;
    
    if(sqrt((data(1)-7)^2 + (data(2)-1)^2) <= 1 )
        class1 = 1;
    end
    
    if( (data(1) > 3 && data(1) < 5) && (data(2) > 3 && data(2) < 5) )
        class2 = 1;
    end
    
    if( ( (3 - data(1)) * (0 - 0) - (7 - 3) * (0 - data(2)) >= 0 ) && ( (7 - data(1)) * (4 - 0) - (5 - 7) * (0 - data(2)) >= 0) && ( (5 - data(1)) * (0 - 4) - (3 - 5) * (4 - data(2))  >= 0) )
        class3 = 1;
    end
    
    if(class1==1 && class2==0 && class3==0)
        classIndex=1;
    elseif (class1==0 && class2==1 && class3==0)
        classIndex=2;
    elseif( class1==0 && class2==0 && class3==1)
        classIndex=3;
    elseif(class1==1 && class2==1 && class3==0)
        classIndex=2;
    elseif(class1==1 && class2==0 && class3==1)
        classIndex=3;
    elseif(class1==0 && class2==1 && class3==1)
        classIndex=3;
    elseif(class1==1 && class2==1 && class3==1)
        classIndex=3;
    else
        classIndex=4;
    end
end

