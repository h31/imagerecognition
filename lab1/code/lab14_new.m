close all;

% Нормальное распределение
%% Непарметрическая классификация
 asixVex = [-2 5 -4 2];
xSize = -6:0.2:6;
ySize = -6:0.2:6;
[X, Y]= meshgrid(xSize, ySize);
% Способ 1
h1_1 = 1.06*std(global_1_r1(:,1))*length(global_1_r1)^(-1/5);
h2_1 = 1.06*std(global_1_r1(:,2))*length(global_1_r1)^(-1/5);
h1_2 = 1.06*std(global_1_r2(:,1))*length(global_1_r2)^(-1/5);
h2_2 = 1.06*std(global_1_r2(:,2))*length(global_1_r2)^(-1/5);
h1_3 = 1.06*std(global_1_r3(:,1))*length(global_1_r3)^(-1/5);
h2_3 = 1.06*std(global_1_r3(:,2))*length(global_1_r3)^(-1/5);
% Способ 2
temp = sort(global_1_r1(1:length(global_1_r1),1));
IQR = temp(0.75*length(global_1_r1)) - temp(0.25*length(global_1_r1));
h1_1_Type2 = 0.9*min(std(global_1_r1(:,1)),IQR/1.34)*length(global_1_r1)^(-1/5);
temp = sort(global_1_r1(1:length(global_1_r1),2));
IQR = temp(0.75*length(global_1_r1)) - temp(0.25*length(global_1_r1));
h2_1_Type2 = 0.9*min(std(global_1_r1(:,2)),IQR/1.34)*length(global_1_r1)^(-1/5);
temp = sort(global_1_r2(1:length(global_1_r2),1));
IQR = temp(0.75*length(global_1_r2)) - temp(0.25*length(global_1_r2));
h1_2_Type2 = 0.9*min(std(global_1_r2(:,1)),IQR/1.34)*length(global_1_r2)^(-1/5);
temp = sort(global_1_r2(1:length(global_1_r2),2));
IQR = temp(0.75*length(global_1_r2)) - temp(0.25*length(global_1_r2));
h2_2_Type2 = 0.9*min(std(global_1_r2(:,2)),IQR/1.34)*length(global_1_r2)^(-1/5);
temp = sort(global_1_r3(1:length(global_1_r3),1));
IQR = temp(0.75*length(global_1_r3)) - temp(0.25*length(global_1_r3));
h1_3_Type2 = 0.9*min(std(global_1_r3(:,1)),IQR/1.34)*length(global_1_r3)^(-1/5);
temp = sort(global_1_r3(1:length(global_1_r3),2));
IQR = temp(0.75*length(global_1_r3)) - temp(0.25*length(global_1_r3));
h2_3_Type2 = 0.9*min(std(global_1_r3(:,2)),IQR/1.34)*length(global_1_r3)^(-1/5);

% Способ 3
A = -eye(2);
B = [0;0];
[h_1_Type3, fVal] = fmincon('optFun1',[h1_1; h2_1],A,B);
[h_2_Type3, fVal] = fmincon('optFun2',[h1_2; h2_2],A,B);
[h_3_Type3, fVal] = fmincon('optFun3',[h1_3; h2_3],A,B);


nonParametricGaus =  ones(length(xSize),length(ySize));
nonParametricGaus2 = nonParametricGaus;
nonParametricGaus3 = nonParametricGaus;
nonParametricGaus4 = nonParametricGaus;
nonParametricGaus5 = nonParametricGaus;
prob = ones(length(xSize),length(ySize),3);
prob2 = prob;
prob3 = prob;
prob4 = prob;
prob5 = prob;
h = waitbar(0,'Classification ...');
for i=1:length(xSize)
    for j=1:length(ySize)
        nonParametricGaus(i,j) = kernelClassification(1,1,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);     
        temp = kernelClassification(2,1,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);
        prob(i,j,1) = temp(1);
        prob(i,j,2) = temp(2);
        prob(i,j,3) = temp(3);
        nonParametricGaus2(i,j) = kernelClassification(1,1,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h1_1_Type2,h2_1_Type2,h1_2_Type2,h2_2_Type2,h1_3_Type2,h2_3_Type2);
        temp = kernelClassification(2,1,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h1_1_Type2,h2_1_Type2,h1_2_Type2,h2_2_Type2,h1_3_Type2,h2_3_Type2);
        prob2(i,j,1) = temp(1);
        prob2(i,j,2) = temp(2);
        prob2(i,j,3) = temp(3);
        nonParametricGaus3(i,j) = kernelClassification(1,1,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h_1_Type3(1),h_1_Type3(2),h_2_Type3(1),h_2_Type3(2),h_3_Type3(1),h_3_Type3(2));
        temp = kernelClassification(2,1,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h_1_Type3(1),h_1_Type3(2),h_2_Type3(1),h_2_Type3(2),h_3_Type3(1),h_3_Type3(2));
        prob3(i,j,1) = temp(1);
        prob3(i,j,2) = temp(2);
        prob3(i,j,3) = temp(3);
        nonParametricGaus4(i,j) = kernelClassification(1,2,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);
        temp = kernelClassification(2,2,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);
        prob4(i,j,1) = temp(1);
        prob4(i,j,2) = temp(2);
        prob4(i,j,3) = temp(3);       
        nonParametricGaus5(i,j) = kernelClassification(1,3,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);
        temp = kernelClassification(2,3,[X(i,j); Y(i,j)],global_1_r1,global_1_r2,global_1_r3,...
                    h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);
        prob5(i,j,1) = temp(1);
        prob5(i,j,2) = temp(2);
        prob5(i,j,3) = temp(3);        
    end
    waitbar(i/length(xSize),h,sprintf('Classification %.0f%%.',i/length(xSize)*100));
end
close(h);

% Finding error
Test_data = [global_1_r1; global_1_r2; global_1_r3];
failClassify1  = 0;
failClassify2  = 0;
failClassify3  = 0;
failClassify4  = 0;
failClassify5  = 0;
h = waitbar(0,'Calculating error ...');
for i = 1:length(Test_data)
    tempClassify1 = kernelClassification(1,1,Test_data(i,:),global_1_r1,global_1_r2,global_1_r3,...
                        h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);
    tempClassify2 = kernelClassification(1,1,Test_data(i,:),global_1_r1,global_1_r2,global_1_r3,...
                        h1_1_Type2,h2_1_Type2,h1_2_Type2,h2_2_Type2,h1_3_Type2,h2_3_Type2);
    tempClassify3 = kernelClassification(1,1,Test_data(i,:),global_1_r1,global_1_r2,global_1_r3,...
                    h_1_Type3(1),h_1_Type3(2),h_2_Type3(1),h_2_Type3(2),h_3_Type3(1),h_3_Type3(2));
    tempClassify4 = kernelClassification(1,2,Test_data(i,:),global_1_r1,global_1_r2,global_1_r3,...
                    h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);
    tempClassify5 = kernelClassification(1,3,Test_data(i,:),global_1_r1,global_1_r2,global_1_r3,...
                    h1_1,h2_1,h1_2,h2_2,h1_3,h2_3);
    if(i<=length(global_1_r1))
        if(tempClassify1 ~= 2)
            failClassify1 = failClassify1 + 1;
        end
        if(tempClassify2 ~= 2)
            failClassify2 = failClassify2 + 1;
        end
        if(tempClassify3 ~= 2)
            failClassify3 = failClassify3 + 1;
        end
        if(tempClassify4 ~= 2)
            failClassify4 = failClassify4 + 1;
        end
        if(tempClassify5 ~= 2)
            failClassify5 = failClassify5 + 1;
        end
    end
    if(i>=length(global_1_r1) +1 && i <= length(global_1_r1)+length(global_1_r2))
        if(tempClassify1 ~= 3)
            failClassify1 = failClassify1 + 1;
        end
        if(tempClassify2 ~= 3)
            failClassify2 = failClassify2 + 1;
        end
        if(tempClassify3 ~= 3)
            failClassify3 = failClassify3 + 1;
        end
        if(tempClassify4 ~= 3)
            failClassify4 = failClassify4 + 1;
        end
        if(tempClassify5 ~= 3)
            failClassify5 = failClassify5 + 1;
        end
    end
    if(i>=length(global_1_r1)+length(global_1_r2)+1 && i <= length(global_1_r1)+length(global_1_r2)+length(global_1_r3))
        if(tempClassify1 ~= 4)
            failClassify1 = failClassify1 + 1;
        end
        if(tempClassify2 ~= 4)
            failClassify2 = failClassify2 + 1;
        end
        if(tempClassify3 ~= 4)
            failClassify3 = failClassify3 + 1;
        end
        if(tempClassify4 ~= 4)
            failClassify4 = failClassify4 + 1;
        end
        if(tempClassify5 ~= 4)
            failClassify5 = failClassify5 + 1;
        end
    end
    waitbar(i/length(Test_data),h,sprintf('Calculating error %.0f%%.',i/length(Test_data)*100));
end
close(h);

fprintf('Точность непарам. классиф. для гаусовых областей (h - std) %0.f%%\n',...
            (length(Test_data)-failClassify1)/length(Test_data)*100);
fprintf('Точность непарам. классиф. для гаусовых областей (h - IQR) %0.f%%\n',...
            (length(Test_data)-failClassify2)/length(Test_data)*100);
fprintf('Точность непарам. классиф. для гаусовых областей (h - cross (Gaus)) %0.f%%\n',...
            (length(Test_data)-failClassify3)/length(Test_data)*100);
fprintf('Точность непарам. классиф. для гаусовых областей (kernl - Gaus) %0.f%%\n',...
            (length(Test_data)-failClassify4)/length(Test_data)*100);
fprintf('Точность непарам. классиф. для гаусовых областей (наивн. Байеса) %0.f%%\n',...
            (length(Test_data)-failClassify5)/length(Test_data)*100);

        
figure(1);
subplot(2,1,1)
axis(asixVex);
hold on;
contourf(X,Y,nonParametricGaus,'LineStyle','none');
plot(global_1_r1(:,1),global_1_r1(:,2), 'ro',global_1_r2(:,1),...
    global_1_r2(:,2),'bo',global_1_r3(:,1),global_1_r3(:,2),'go');
% title('Непарам. классиф. для гаусовых областей (h - std)');
hold off;
subplot(2,1,2)
axis(asixVex);
hold on;
surf(X,Y, prob(:,:,1),'FaceColor','r');
surf(X,Y, prob(:,:,2),'FaceColor','b');
surf(X,Y, prob(:,:,3),'FaceColor','g');
surf(X,Y, zeros(length(xSize),length(ySize)),'EdgeColor','w','FaceColor','w');
grid on;
hold off;

figure(2);
subplot(2,1,1)
axis(asixVex);
hold on;
contourf(X,Y,nonParametricGaus2,'LineStyle','none');
plot(global_1_r1(:,1),global_1_r1(:,2), 'ro',global_1_r2(:,1),...
    global_1_r2(:,2),'bo',global_1_r3(:,1),global_1_r3(:,2),'go');
% title('Непарам. классиф. для гаусовых областей (h - IQR)');
hold off;
subplot(2,1,2)
axis(asixVex);
hold on;
surf(X,Y, prob2(:,:,1),'FaceColor','r');
surf(X,Y, prob2(:,:,2),'FaceColor','b');
surf(X,Y, prob2(:,:,3),'FaceColor','g');
surf(X,Y, zeros(length(xSize),length(ySize)),'EdgeColor','w','FaceColor','w');
grid on;
hold off;

figure(3);
subplot(2,1,1)
axis(asixVex);
hold on;
contourf(X,Y,nonParametricGaus3,'LineStyle','none');
plot(global_1_r1(:,1),global_1_r1(:,2), 'ro',global_1_r2(:,1),...
    global_1_r2(:,2),'bo',global_1_r3(:,1),global_1_r3(:,2),'go');
% title('Непарам. классиф. для гаусовых областей (h - cross (Gaus))');
hold off;
subplot(2,1,2)
axis(asixVex);
hold on;
surf(X,Y, prob3(:,:,1),'FaceColor','r');
surf(X,Y, prob3(:,:,2),'FaceColor','b');
surf(X,Y, prob3(:,:,3),'FaceColor','g');
surf(X,Y, zeros(length(xSize),length(ySize)),'EdgeColor','w','FaceColor','w');
grid on;
hold off;

figure(4);
subplot(2,1,1)
axis(asixVex);
hold on;
contourf(X,Y,nonParametricGaus4,'LineStyle','none');
plot(global_1_r1(:,1),global_1_r1(:,2), 'ro',global_1_r2(:,1),...
    global_1_r2(:,2),'bo',global_1_r3(:,1),global_1_r3(:,2),'go');
% title('Непарам. классиф. для гаусовых областей (kernl - Gaus)');
hold off;
subplot(2,1,2)
axis(asixVex);
hold on;
surf(X,Y, prob4(:,:,1),'FaceColor','r');
surf(X,Y, prob4(:,:,2),'FaceColor','b');
surf(X,Y, prob4(:,:,3),'FaceColor','g');
surf(X,Y, zeros(length(xSize),length(ySize)),'EdgeColor','w','FaceColor','w');
grid on;
hold off;

figure(5);
subplot(2,1,1)
axis(asixVex);
hold on;
contourf(X,Y,nonParametricGaus5,'LineStyle','none');
plot(global_1_r1(:,1),global_1_r1(:,2), 'ro',global_1_r2(:,1),...
    global_1_r2(:,2),'bo',global_1_r3(:,1),global_1_r3(:,2),'go');
% title('Непарам. классиф. для гаусовых областей (наивн. Байеса)');
hold off;
subplot(2,1,2)
axis(asixVex);
hold on;
surf(X,Y, prob5(:,:,1),'FaceColor','r');
surf(X,Y, prob5(:,:,2),'FaceColor','b');
surf(X,Y, prob5(:,:,3),'FaceColor','g');
surf(X,Y, zeros(length(xSize),length(ySize)),'EdgeColor','w','FaceColor','w');
grid on;
hold off;