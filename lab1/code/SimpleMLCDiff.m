function classIndex = SimpleMLCDiff( data, mu1, sigma1, mu2,sigma2,mu3,sigma3, apr)
        probabilityClass1 = apr(1) * 1/(2*pi*sqrt(det(sigma1)))*exp(-((data-mu1)*inv(sigma1)*(data-mu1)')/2);
        probabilityClass2 = apr(2) * 1/(2*pi*sqrt(det(sigma2)))*exp(-((data-mu2)*inv(sigma2)*(data-mu2)')/2);
        probabilityClass3 = apr(3) * 1/(2*pi*sqrt(det(sigma3)))*exp(-((data-mu3)*inv(sigma3)*(data-mu3)')/2);
        [maxValue, index] = max([probabilityClass1 probabilityClass2 probabilityClass3]);
        classIndex = index;
end