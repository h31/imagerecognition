function index = SimpleKNNTwo( data, arr1, arr2, counter )
indexArr = zeros(1, size(arr1,1)+size(arr2,1));
templateArr = [arr1; arr2];

    for i=1:1:size(arr1)
        indexArr(i)=1;
    end
    
    for i=size(arr1)+1:1:size(arr1)+size(arr2)
        indexArr(i)=2;
    end

    firstClass = 0;
    secondClass = 0;

    for i=1:1:counter
        minIndex = 1;
        for j=1:1:size(templateArr)
            current = sqrt((data(1)-templateArr(j,1))^2 + (data(2)-templateArr(j,2))^2);
            minIndexVal = sqrt((data(1)-templateArr(minIndex,1))^2 + (data(2)-templateArr(minIndex,2))^2);
            if (minIndexVal > current)
                minIndex = j;
            end
        end
        
        if(indexArr(minIndex) == 1)
            firstClass = firstClass + 1;
        else
            secondClass = secondClass + 1;
        end
        
        templateArr(minIndex,1) = 1000;
        templateArr(minIndex,2) = 1000;
    end
    
    [maxValue, index] = max([firstClass secondClass]);
end

