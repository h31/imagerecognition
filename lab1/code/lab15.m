% % Классификация для распределения Гаусса методом ближайших соседей
% mesh = [-4:0.2:5];
% [X1,X2] = meshgrid(mesh,mesh);
% Z=zeros(size(X1));
% for i=1:1:size(X1)
%     for j = 1:1:size(X2)
%         display(i);
%         Z(i,j) = SimpleKNN([X1(i,j) X2(i,j)], global_1_r1,global_1_r2,global_1_r3,1);
%     end
% end
% figure
% hold on
% colormap([0 0 1; 1 0 0; 0 1 0]);
% contourf(X1,X2,Z,[1 2 3],'linestyle','none');
% plot(global_1_r1(:, 1), global_1_r1(:, 2), 'y*');
% plot(global_1_r2(:, 1), global_1_r2(:, 2), 'm*');
% plot(global_1_r3(:, 1), global_1_r3(:, 2), 'c*');
% axis([-2 5 -4 2]);

% % Классификация для случайного распределения методом ближайших соседей
% mesh = [0:0.5:8];
% [X1,X2] = meshgrid(mesh,mesh);
% Z=zeros(size(X1));
% for i=1:1:size(X1)
%     for j = 1:1:size(X2)
%         display(i);
%         Z(i,j) = SimpleKNN([X1(i,j) X2(i,j)], global_2_r1,global_2_r2,global_2_r3,7);
%     end
% end
% figure
% hold on
% colormap([0 0 1; 1 0 0; 0 1 0]);
% axis([2 8 0 6]);
% contourf(X1,X2,Z,[1 2 3],'linestyle','none');
% hold on;
% plot(global_2_r1(:,1),global_2_r1(:,2),'y+');
% hold on;
% plot(global_2_r2(:,1),global_2_r2(:,2),'mo');
% hold on;
% plot(global_2_r3(:,1),global_2_r3(:,2),'c*');

% % Классификация для Гауссовой смеси методом ближайших соседей
% mesh = [-5:1:20];
% [X1,X2] = meshgrid(mesh,mesh);
% Z=zeros(size(X1));
% for i=1:1:size(X1)
%     display(i);
%     for j = 1:1:size(X2)
%         Z(i,j) = SimpleKNN([X1(i,j) X2(i,j)], global_3_r1,global_3_r2,global_3_r3,7);
%     end
% end
% figure
% hold on
% colormap([1 0 0; 0 1 0; 0 0 1]);
% contourf(X1,X2,Z,[1 2 3],'linestyle','none');
% plot(global_3_r1(:,1),global_3_r1(:,2),'y+');
% plot(global_3_r2(:,1),global_3_r2(:,2),'mo');
% plot(global_3_r3(:,1),global_3_r3(:,2),'c*');

% Классификация для собственного распределения методом ближайших соседей
mesh = [1:0.1:4];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
for j = 1:1:size(X2)
Z(i,j) = SimpleKNNTwo([X1(i,j) X2(i,j)], global_4_r1,global_4_r2,7);
end
end
figure
hold on
colormap([0 0 1; 1 0 0]);
contourf(X1,X2,Z,[1 2],'linestyle','none');
plot(global_4_r1(:,1),global_4_r1(:,2),'y+');
plot(global_4_r2(:,1),global_4_r2(:,2),'mo');

% % Линейный классификатор со взешиванием
% figure;
% title('Линейный классификатор со взешиванием');
% hold on
% gscatter(training(:,1),training(:,2),class,'brg','+xo');
% hold on;
% sample = unifrnd(-6, 6, 600, 2);
% c3 = knnclassify(sample, training, class, 3);
% gscatter(sample(:,1),sample(:,2),c3,'mcy','+xo');
% legend('Training group 1', 'Training group 2', 'Training group 3','Data in group 1','Data in group 2','Data in group 3');