close all;
clear all;

dots_number = 200;

% Распределение Гаусса
mu1 = [1 0];
sigma1 = [.4 .0; .0 .4];
r1 = mvnrnd(mu1, sigma1, dots_number);
global_1_r1 = r1;
global_1_mu1 = mu1;
global_1_sigma1 = sigma1;

mu2 = [0 -2];
r2 = mvnrnd(mu2, sigma1, dots_number);
global_1_r2 = r2;
global_1_mu2 = mu2;
global_1_sigma2 = sigma1;

mu3 = [3 -1];
r3 = mvnrnd(mu3, sigma1, dots_number);
global_1_r3 = r3;
global_1_mu3 = mu3;
global_1_sigma3 = sigma1;

figure;
hold on;
grid on;
axis([-2 5 -4 2]);
plot(r1(:, 1), r1(:, 2), 'b*');
plot(r2(:, 1), r2(:, 2), 'r*');
plot(r3(:, 1), r3(:, 2), 'g*');

C = [1-0.9973 1-0.9545 1-0.6827];
x1 = -3:.1:7; x2 = -5:.1:4;
[X1,X2] = meshgrid(x1,x2);

F1 = mvnpdf([X1(:) X2(:)],mu1,sigma1);
F1 = reshape(F1,length(x2),length(x1));
contour(x1,x2,F1,C,'color','b');

F2 = mvnpdf([X1(:) X2(:)],mu2,sigma1);
F2 = reshape(F2,length(x2),length(x1));
contour(x1,x2,F2,C,'color','r');

F3 = mvnpdf([X1(:) X2(:)],mu3,sigma1);
F3 = reshape(F3,length(x2),length(x1));
contour(x1,x2,F3,C,'color','g');

% генерация разомкнутых тестовых областей

r1 = 10.*rand(2,10000);
r2 = 10.*rand(2,10000);
r3 = 15.*rand(2,100000);

% Распределенеи ограничено окружностями
ang=0:0.01:2*pi; 
xp=1*cos(ang);
yp=1*sin(ang);

size_r1 = 1;
for i=1:1:10000
    if(sqrt((r1(1,i)-7)^2 + (r1(2,i)-1)^2) <= 1)
        r1_1(size_r1,1) = r1(1,i);
        r1_1(size_r1,2) = r1(2,i);
        size_r1 = size_r1 + 1;
    end
end

global_2_r1 = r1_1;

% Распределение ограничено квадратными областями
cube1_x = [3,5,5,3];
cube1_y = [3,3,5,5];

size_r2 = 1;
for i=1:1:10000
    if( ( (r2(1,i) > 3 && r2(1,i) < 5) && (r2(2,i) > 3 && r2(2,i) < 5) ) )
        r2_1(size_r2,1) = r2(1,i);
        r2_1(size_r2,2) = r2(2,i);
        size_r2 = size_r2 + 1;
    end
end

global_2_r2=r2_1;

% Распределение ограничено треугольными областями
tr1_x = [3,7,5];
tr1_y = [0,0,4];

size_r3 = 1;
for i=1:1:10000
    if( ( (3 - r3(1,i)) * (0 - 0) - (7 - 3) * (0 - r3(2,i)) >= 0 ) && ( (7 - r3(1,i)) * (4 - 0) - (5 - 7) * (0 - r3(2,i)) >= 0) && ( (5 - r3(1,i)) * (0 - 4) - (3 - 5) * (4 - r3(2,i))  >= 0) )
        r3_1(size_r3,1) = r3(1,i);
        r3_1(size_r3,2) = r3(2,i);
        size_r3 = size_r3 + 1;
    end
end

global_2_r3=r3_1;

figure
patch(7+xp,1+yp,'r');
hold on;
patch(cube1_x,cube1_y,'g');
hold on;
patch(tr1_x,tr1_y,'b');
hold on;
plot(r1_1(:,1),r1_1(:,2),'+');
hold on;
plot(r2_1(:,1),r2_1(:,2),'o');
hold on;
plot(r3_1(:,1),r3_1(:,2),'*');

% Гауссова смесь
gmdistribution
global_3_mu1_1 = [2,3];
global_3_mu1_2 = [-1,-1];
global_3_mu2_1 = [4,9];
global_3_mu2_2 = [8,11];
global_3_mu3_1 = [13,4];
global_3_mu3_2 = [15,5];

global_3_sigma11 = [2 0; 0 .5];
global_3_sigma12 = [1 0; 0 1];
global_3_sigma21 = [2 0; 0 .2];
global_3_sigma22 = [5 0; 0 .8];
global_3_sigma31 = [4 0; 0 .4];
global_3_sigma32 = [2 0; 0 .3];

global_3_r1 = [mvnrnd(global_3_mu1_1,global_3_sigma11,100); mvnrnd(global_3_mu1_2,global_3_sigma12,100)];
global_3_r2 = [mvnrnd(global_3_mu2_1,global_3_sigma21,100); mvnrnd(global_3_mu2_2,global_3_sigma22,100)];
global_3_r3 = [mvnrnd(global_3_mu3_1,global_3_sigma31,100); mvnrnd(global_3_mu3_2,global_3_sigma32,100)];

options = statset('Display','final');

figure;

plot(global_3_r1(:,1),global_3_r1(:,2),'b+');
hold on;
obj1 = fitgmdist(global_3_r1,2,'Options',options);
h1 = ezcontour(@(x,y)pdf(obj1,[x y]),[-5 7],[-6 6],30);

plot(global_3_r2(:,1),global_3_r2(:,2),'ro');
obj2 = fitgmdist(global_3_r2,2,'Options',options);
h2 = ezcontour(@(x,y)pdf(obj2,[x y]),[0 15],[7 13]);

plot(global_3_r3(:,1),global_3_r3(:,2),'g*');
obj3 = fitgmdist(global_3_r3,2,'Options',options);
h3 = ezcontour(@(x,y)pdf(obj3,[x y]),[5 20],[0 10]);
axis([-5 20 -5 20]);

% Случайное распределение
global_4_r1 = rand(200,2)*[1 0; 0 1];
global_4_r2 = rand(200,2)*[1 0; 0 1];

for i=1:1:50
    global_4_r1(i,:)=global_4_r1(i,:)+[1 1];
    global_4_r2(i,:)=global_4_r2(i,:)+[2 1];
end

for i=51:1:100
    global_4_r1(i,:)=global_4_r1(i,:)+[1 2];
    global_4_r2(i,:)=global_4_r2(i,:)+[3 1];
end

for i=101:1:150
    global_4_r1(i,:)=global_4_r1(i,:)+[1 3];
    global_4_r2(i,:)=global_4_r2(i,:)+[3 2];
end

for i=151:1:200
    global_4_r1(i,:)=global_4_r1(i,:)+[2 2];
    global_4_r2(i,:)=global_4_r2(i,:)+[3 3];
end
    
figure
plot(global_4_r1(:,1),global_4_r1(:,2),'+',global_4_r2(:,1),global_4_r2(:,2),'o');