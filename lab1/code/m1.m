clear all;
close all;
mu1 = [2,3]; mu2 = [5,6]; mu3 = [4,9];
ro = 0;
sigx = 1;
sigy = 1;
sigma = [sigx^2,sigx*sigy*ro;sigx*sigy*ro,sigy^2];
dot_num = 200;
r1 = mvnrnd(mu1,sigma,dot_num);
r2 = mvnrnd(mu2,sigma,dot_num);
r3 = mvnrnd(mu3,sigma,dot_num);
figure
plot(r1(:,1),r1(:,2),'+r')
hold on
plot(r2(:,1),r2(:,2),'*g')
hold on
plot(r3(:,1),r3(:,2),'.b')

sigma1=sigx;sigma2=sigy;MX1=mu1(1);MX2=mu1(2);r=0.02;
mesh = [-3:0.05:15];
[X1 X2]=meshgrid(mesh, mesh);
F1 = mvnpdf([X1(:) X2(:)],mu1,sigma);
F1 = reshape(F1,length(mesh),length(mesh));

F2 = mvnpdf([X1(:) X2(:)],mu2,sigma);
F2 = reshape(F2,length(mesh),length(mesh));

F3 = mvnpdf([X1(:) X2(:)],mu3,sigma);
F3 = reshape(F3,length(mesh),length(mesh));

task2 = [rand(200,1)*5 rand(200,1)*5];
fig1=a(a>0.2 & a<0.7); 


i = [1 2 3];

coeff = 1/(2*pi*det(sigma)^(1/2))*exp(-(i.^2)/2);

contour(mesh,mesh,F1,[coeff(1) coeff(2) coeff(3)],'+r');
contour(mesh,mesh,F2,[coeff(1) coeff(2) coeff(3)],'*g');
contour(mesh,mesh,F3,[coeff(1) coeff(2) coeff(3)],'.b');

w(1,:) = mu1/sigx;
w(2,:) = mu2/sigx;
w(3,:) = mu3/sigx;

w0(1) = -(1/(sigx^2))*mu1*(mu1')
w0(2) = -(1/(sigx^2))*mu2*(mu2')
w0(3) = -(1/(sigx^2))*mu3*(mu3')

x = [0,10];
eq(1) = (w0(1)-w(1,1)*x(1))/w(1,2)
eq(2) = (w0(1)-w(1,1)*x(2))/w(1,2)
plot(x,eq)





for i = 1:size(X1)
    for j = 1:size(X2)
        Z(i,j) = SimpleMLC([X1(i,j) X2(i,j)], mu1, sigx, mu2,sigx,mu3,sigx);
    end
end

for i = 1:size(X1)
    for j = 1:size(X2)
        Z2(i,j) = SimpleMAP([X1(i,j) X2(i,j)], mu1, mu2, mu3, sigx, [0.1 0.5 0.7]);
    end
end

subplot(2,1,1);
colormap([1 0 0; 0 0 1 ;0 1 0]);
surf(X1,X2,Z2,'edgecolor','none');
view(2);

subplot(2,1,2);
contourf(X1,X2,Z2,[1 2 3], 'fill', 'off','linewidth', 2);
hold on;
plot(r1(:,1),r1(:,2),'+r')
hold on
plot(r2(:,1),r2(:,2),'*g')
hold on
plot(r3(:,1),r3(:,2),'.b')
% plot(x(c==1,1), x(c==1,2), 'linestyle','none', 'color','b', 'marker', 'o');
% plot(x(c==0,1), x(c==0,2), 'linestyle','none', 'color','r', 'marker', 'o');