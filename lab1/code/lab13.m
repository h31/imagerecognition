close all;

[C1,sigma1] = countMMP(global_1_r1);
[C2,sigma2] = countMMP(global_1_r2);
[C3,sigma3] = countMMP(global_1_r3);

%Встроенными функциями
figure
lab1_3_1(global_1_mu1, global_1_sigma1, global_1_r1);
lab1_3_1(global_1_mu2, global_1_sigma2, global_1_r2);
lab1_3_1(global_1_mu3, global_1_sigma3, global_1_r3);


r1 = mvnrnd(mu1,sigma1,dots_number);
r2 = mvnrnd(mu2,sigma2,dots_number);
r3 = mvnrnd(mu3,sigma3,dots_number);

% Гауссова смесь
lab1_3_2(global_3_mu1_1,global_3_mu1_2, global_3_sigma11,global_3_sigma12, global_3_r1);
lab1_3_2(global_3_mu2_1,global_3_mu2_2, global_3_sigma21,global_3_sigma22, global_3_r2);
lab1_3_2(global_3_mu3_1,global_3_mu3_2, global_3_sigma31,global_3_sigma32, global_3_r3);

%Классификация данных по параметрическим оценкам
x1 = -3:.1:7; x2 = -5:.1:4;
[X1,X2] = meshgrid(x1,x2);
F = mvnpdf([X1(:) X2(:)],mu1,sigma1);
F = reshape(F,length(x2),length(x1));

% Признаки независимы
figure
hold on;
axis([-6 6 -6 6]);
plot(r1(:,1),r1(:,2),'b*');
hold on;
mvncdf([0 0],[1 1],mu1,sigma1);
h = contour(x1,x2,F,[0.028,0.0063,0.0005],'color','b');
xlabel('x'); ylabel('y');

F = mvnpdf([X1(:) X2(:)],mu2,sigma2);
F = reshape(F,length(x2),length(x1));

plot(r2(:,1),r2(:,2),'r*');
hold on;
mvncdf([0 0],[1 1],mu2,sigma2);
h = contour(x1,x2,F,[0.096,0.01795,0.00147],'color','r');
xlabel('x'); ylabel('y');

F = mvnpdf([X1(:) X2(:)],mu3,sigma3);
F = reshape(F,length(x2),length(x1));
plot(r3(:,1),r3(:,2),'g*');
hold on;
mvncdf([0 0],[1 1],mu3,sigma2);
h = contour(x1,x2,F,[0.018,0.0041,0.00033],'color','g');

% Встроенный классификатор для случая признаки зависимы и дисперсии
% одинаковы
figure;
hold on
axis([-6 6 -6 6]);
class=zeros(1,600);
 for i=1:1:200
     class(i) = 1;
 end
 for i=201:1:400
     class(i) = 2;
 end
 for i=401:1:600
     class(i) = 3;
 end

training = [global_1_r1;global_1_r2;global_1_r3];
group = [ repmat(1,length(global_1_r1),1); repmat(2,length(global_1_r2),1); repmat(3,length(global_1_r3),1) ];
hold on;
mesh = [-6:0.1:6];
[X1,X2] = meshgrid(mesh,mesh);
pairs = [X1(:) X2(:)];
% c3 = knnclassify(sample, training, group, 3);
mdl = fitcknn(training, group, 'NumNeighbors', 3, 'Distance', 'euclidean');
c3 = predict(mdl, pairs);
s = size(mesh)
res = reshape(c3, s(2), s(2));
contourf(X1,X2,res,[1 2 3],'linestyle','none');
%gscatter(sample(:,1),sample(:,2),c3,'mcy','+xo');
gscatter(training(:,1),training(:,2),group,'ymc','+xo');
axis([-2 5 -4 2]);
%legend('Training group 1', 'Training group 2', 'Training group 3','Data in group 1','Data in group 2','Data in group 3');


% Линейный классификатор
figure;
hold on
gscatter(training(:,1),training(:,2),class,'brg','+xo');
hold on;
sample = unifrnd(-6, 6, 3*dots_number, 2);
c3 = classify(sample, training, class, 'diaglinear');
gscatter(sample(:,1),sample(:,2),c3,'mcy','+xo');
%legend('Training group 1', 'Training group 2', 'Training group 3','Data in group 1','Data in group 2','Data in group 3');

%Диаграмма разбиения для нормального распределения TODO
a = -6:0.1:6;
b = -6:0.1:6;
[X,Y] = meshgrid(a,b);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLC([X1(i,j) X2(i,j)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);
    end
end
figure; 
hold on; 
axis([-6 6 -6 6]);
surface(X,Y,Z,'edgecolor','none'); 
view(2);


% Встроенный MAP-классификатор
figure;
hold on
group = [ repmat(1,length(global_1_r1),1); repmat(2,length(global_1_r2),1); repmat(3,length(global_1_r3),1) ];
gscatter(training(:,1),training(:,2),group,'brg','+xo');
hold on;
sample = unifrnd(-6, 6, 3*dots_number, 2);
% c3 = knnclassify(sample, training, group, 3);
mdl = fitcknn(training, group, 'NumNeighbors', 3, 'Distance', 'euclidean');
c3 = predict(mdl, sample);
gscatter(sample(:,1),sample(:,2),c3,'mcy','+xo');
%legend('Training group 1', 'Training group 2', 'Training group 3','Data in group 1','Data in group 2','Data in group 3');

% Линейный MAP-классификатор
figure;
hold on;
mesh = [-6:0.1:6];
[X1,X2] = meshgrid(mesh,mesh);
pairs = [X1(:) X2(:)];
c3 = classify(pairs, training, class, 'diaglinear');

s = size(mesh)
res = reshape(c3, s(2), s(2));
contourf(X1,X2,res,[1 2 3],'linestyle','none');

hold on
gscatter(training(:,1),training(:,2),class,'ymc','+xo');
axis([-2 5 -4 2]);
% legend('Training group 1', 'Training group 2', 'Training group 3','Data in group 1','Data in group 2','Data in group 3');

training = [global_3_r1;global_3_r2;global_3_r3];
class=zeros(1,600);
for i=1:1:200
class(i) = 1;
end
for i=201:1:400
class(i) = 2;
end
for i=401:1:600
class(i) = 3;
end
figure;
hold on;
mesh = [-5:0.1:20];
[X1,X2] = meshgrid(mesh,mesh);
pairs = [X1(:) X2(:)];
c3 = classify(pairs, training, class, 'diaglinear');
s = size(mesh)
res = reshape(c3, s(2), s(2));
contourf(X1,X2,res,[1 2 3],'linestyle','none');
hold on
gscatter(training(:,1),training(:,2),class,'ymc','+xo');

c4 = classify(training, training, class, 'diaglinear');

err = 0;
for i=1:length(c4)
  if c4(i) ~= class(i)
    err = err +1;
  end
end
err = err / length(c4) * 100

% Собственный MAP - классификатор
a = -6:0.1:6;
b = -6:0.1:6;
[X,Y] = meshgrid(a,b);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLC([X1(i,j) X2(i,j)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);
    end
end
figure; 
hold on; 
axis([-6 6 -6 6]);
surface(X,Y,Z,'edgecolor','none'); 
view(2);