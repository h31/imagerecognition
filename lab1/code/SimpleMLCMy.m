function index = SimpleMLCMy( data )
if(data(1) <= 2 && data(1) >= 1 && data(2) >= 1 && data(2) <= 4)
    index = 1;
elseif (data(1) >= 1 && data(1) <= 3 && data (2) >= 2 && data (2) <= 3)
    index = 1;
elseif (data(1) >= 2 && data (1) <= 4 && data (2) >= 1 && data(2) <= 2)
    index = 2;
elseif (data (1) >= 3 && data (1) <= 4 && data (2) >= 1 && data(2) <= 4)
    index = 2;
else
    index = 3;
end
end