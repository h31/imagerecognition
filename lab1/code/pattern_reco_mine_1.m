function pattern_reco_mine_1

x=rand(1000,2)
c = x(:,1) > x(:,2)
x1 = 0:.01:1;
x2 = 0:.01:1;
[X1,X2] = meshgrid(x1,x2);
Z = (X1>X2) + 1;

subplot(2,1,1);
colormap([1 0 0; 0 0 1 ;0 0 1]);
contourf(X1,X2,Z,[1 2]);

subplot(2,1,2);
contourf(X1,X2,Z,[1 2], 'fill', 'off','linewidth', 2);
hold on;
plot(x(c==1,1), x(c==1,2), 'linestyle','none', 'color','b', 'marker', 'o');
plot(x(c==0,1), x(c==0,2), 'linestyle','none', 'color','r', 'marker', 'o');

