function [C, sigma] = countMMP(X)
C = mean(X);
sigma = cov(X);
end