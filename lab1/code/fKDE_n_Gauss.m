function pKDE = fKDE_n_Gauss( N,h1,h2,x1,x2,X1,X2 )

pKDE = 0;
for i=1:1:length(X1)
    
    K1 = 1/(sqrt(2*pi))*exp(-1/2*( ((x1 - X1(i))/h1) )^2);
    K2 = 1/(sqrt(2*pi))*exp(-1/2*( ((x2 - X2(i))/h1) )^2);
    if ( ( x1 ~= X1(i) ) && ( x2 ~= X2(i) ) )
        pKDE = pKDE + K1*K2;
    end
    
end
pKDE = pKDE /(N*h1*h2);

end

