function index = classRiskFunction(data, mu1, sigma1, mu2, sigma2, mu3, sigma3, apr, C,apost1, apost2, apost3)
  prob = ones(3,1);
  prob(1) = apr(1) * 1/(2*pi*sqrt(det(sigma1)))*exp(-((data-mu1)*inv(sigma1)*(data-mu1)')/2);
  prob(2) = apr(2) * 1/(2*pi*sqrt(det(sigma2)))*exp(-((data-mu2)*inv(sigma2)*(data-mu2)')/2);
  prob(3) = apr(3) * 1/(2*pi*sqrt(det(sigma3)))*exp(-((data-mu3)*inv(sigma3)*(data-mu3)')/2);
  newProb = C *prob.*aprior;
  [maxValue, index] = min(newProb);
end