function pKDE = fKDE_n( N,h1,h2,x1,x2,X1,X2 )

pKDE = 0;
for i=1:1:length(X1)
    if ( abs((x1 - X1(i))/h1) < 1/2 )
        K1 = 1;
    else
        K1 = 0;
    end
    if ( abs((x2 - X2(i))/h2) < 1/2 )
        K2 = 1;
    else
        K2 = 0;
    end
    if ( ( x1 ~= X1(i) ) && ( x2 ~= X2(i) ) )
        pKDE = pKDE + K1*K2;
    end
    
end
pKDE = pKDE /(N*h1*h2);

end

