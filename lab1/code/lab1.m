close all

dots_number = 200;

% Распределение Гаусса
mu1 = [1 0];
sigma1 = [.4 .0; .0 .4];
r1 = mvnrnd(mu1, sigma1, dots_number);
global_1_r1 = r1;
global_1_mu1 = mu1;
global_1_sigma1 = sigma1;

mu2 = [1 -2];
r2 = mvnrnd(mu2, sigma1, dots_number);
global_1_r2 = r2;
global_1_mu2 = mu2;
global_1_sigma2 = sigma1;

mu3 = [4 -1];
r3 = mvnrnd(mu3, sigma1, dots_number);
global_1_r3 = r3;
global_1_mu3 = mu3;
global_1_sigma3 = sigma1;

figure;
hold on;
grid on;
axis([-6 6 -6 6]);
plot(r1(:, 1), r1(:, 2), 'b*');
plot(r2(:, 1), r2(:, 2), 'r*');
plot(r3(:, 1), r3(:, 2), 'g*');

C = [1-0.9973 1-0.9545 1-0.6827];
x1 = -3:.1:7; x2 = -5:.1:4;
[X1,X2] = meshgrid(x1,x2);

F1 = mvnpdf([X1(:) X2(:)],mu1,sigma1);
F1 = reshape(F1,length(x2),length(x1));
contour(x1,x2,F1,C,'color','b');

F2 = mvnpdf([X1(:) X2(:)],mu2,sigma1);
F2 = reshape(F2,length(x2),length(x1));
contour(x1,x2,F2,C,'color','r');

F3 = mvnpdf([X1(:) X2(:)],mu3,sigma1);
F3 = reshape(F3,length(x2),length(x1));
contour(x1,x2,F3,C,'color','g');

% генерация разомкнутых тестовых областей

r1 = 10.*rand(2,10000);
r2 = 10.*rand(2,10000);
r3 = 15.*rand(2,100000);

% Распределенеи ограничено окружностями
ang=0:0.01:2*pi; 
xp=1*cos(ang);
yp=1*sin(ang);

size_r1 = 1;
for i=1:1:10000
    if( (sqrt((r1(1,i)-9)^2 + (r1(2,i)-1)^2) <= 1) || (sqrt((r1(1,i)-7)^2 + (r1(2,i)-5)^2) <= 1) )
        r1_1(size_r1,1) = r1(1,i);
        r1_1(size_r1,2) = r1(2,i);
        size_r1 = size_r1 + 1;
    end
end

global_2_r1 = r1_1;

% Распределение ограничено квадратными областями
cube1_x = [6,8,8,6];
cube1_y = [7,7,9,9];
cube2_x = [5.8,8,8,5.8];
cube2_y = [1,1,3,3];

size_r2 = 1;
for i=1:1:10000
    if( ( (r2(1,i) > 6 && r2(1,i) < 8) && (r2(2,i) > 7 && r2(2,i) < 9) ) || ( (r2(1,i) > 5.8 && r2(1,i) < 8) && (r2(2,i) > 1 && r2(2,i) < 3) ) )
        r2_1(size_r2,1) = r2(1,i);
        r2_1(size_r2,2) = r2(2,i);
        size_r2 = size_r2 + 1;
    end
end

global_2_r2=r2_1;

% Распределение ограничено треугольными областями
tr1_x = [3,7,5];
tr1_y = [0,0,4];
tr2_x = [9,13,11];
tr2_y = [3,3,7];

size_r3 = 1;
for i=1:1:10000
    if( ( (3 - r3(1,i)) * (0 - 0) - (7 - 3) * (0 - r3(2,i)) >= 0 ) && ( (7 - r3(1,i)) * (4 - 0) - (5 - 7) * (0 - r3(2,i)) >= 0) && ( (5 - r3(1,i)) * (0 - 4) - (3 - 5) * (4 - r3(2,i))  >= 0) )
        r3_1(size_r3,1) = r3(1,i);
        r3_1(size_r3,2) = r3(2,i);
        size_r3 = size_r3 + 1;
    elseif ( ( (9 - r3(1,i)) * (3 - 3) - (13 - 9) * (3 - r3(2,i)) >= 0 ) && ( (13 - r3(1,i)) * (7 - 3) - (11 - 13) * (3 - r3(2,i)) >=0 ) && ( (11 - r3(1,i)) * (3 - 7) - (9 - 11) * (7 - r3(2,i)) >= 0) )
        r3_1(size_r3,1) = r3(1,i);
        r3_1(size_r3,2) = r3(2,i);
        size_r3 = size_r3 + 1;
    end
end

global_2_r3=r3_1;

figure
patch(9+xp,1+yp,'r');
hold on;
patch(7+xp,5+yp,'r');
hold on;
plot(r1_1(:,1),r1_1(:,2),'+');
hold on;
patch(cube1_x,cube1_y,'g');
hold on;
patch(cube2_x,cube2_y,'g');
hold on;
plot(r2_1(:,1),r2_1(:,2),'o');
hold on;
patch(tr1_x,tr1_y,'b');
hold on;
patch(tr2_x,tr2_y,'b');
hold on;
plot(r3_1(:,1),r3_1(:,2),'*');

% Гауссова смесь
gmdistribution
global_3_mu1_1 = [2,3];
global_3_mu1_2 = [-1,-1];
global_3_mu2_1 = [8,9];
global_3_mu2_2 = [10,11];
global_3_mu3_1 = [13,5];
global_3_mu3_2 = [15,6];

global_3_sigma11 = [2 0; 0 .5];
global_3_sigma12 = [1 0; 0 1];
global_3_sigma21 = [2 0; 0 .2];
global_3_sigma22 = [5 0; 0 .8];
global_3_sigma31 = [4 0; 0 .4];
global_3_sigma32 = [2 0; 0 .3];

global_3_r1 = [mvnrnd(global_3_mu1_1,global_3_sigma11,100);mvnrnd(global_3_mu1_2,global_3_sigma12,100)];
global_3_r2 = [mvnrnd(global_3_mu2_1,global_3_sigma21,100);mvnrnd(global_3_mu2_2,global_3_sigma22,100)];
global_3_r3 = [mvnrnd(global_3_mu3_1,global_3_sigma31,100);mvnrnd(global_3_mu3_2,global_3_sigma32,100)];

options = statset('Display','final');

figure;

plot(global_3_r1(:,1),global_3_r1(:,2),'b+');
hold on;
obj1 = fitgmdist(global_3_r1,2,'Options',options);
h1 = ezcontour(@(x,y)pdf(obj1,[x y]),[-5 7],[-6 6]);

plot(global_3_r2(:,1),global_3_r2(:,2),'ro');
obj2 = fitgmdist(global_3_r2,2,'Options',options);
h2 = ezcontour(@(x,y)pdf(obj2,[x y]),[0 15],[7 13]);

plot(global_3_r3(:,1),global_3_r3(:,2),'g*');
obj3 = fitgmdist(global_3_r3,2,'Options',options);
h3 = ezcontour(@(x,y)pdf(obj3,[x y]),[5 20],[0 10]);
axis([-5 20 -5 20]);

% Случайное распределение
global_4_r1 = rand(200,2)*[1 0; 0 1];
global_4_r2 = rand(200,2)*[1 0; 0 1];

for i=1:1:50
    global_4_r1(i,:)=global_4_r1(i,:)+[1 1];
    global_4_r2(i,:)=global_4_r2(i,:)+[2 1];
end

for i=51:1:100
    global_4_r1(i,:)=global_4_r1(i,:)+[1 2];
    global_4_r2(i,:)=global_4_r2(i,:)+[3 1];
end

for i=101:1:150
    global_4_r1(i,:)=global_4_r1(i,:)+[1 3];
    global_4_r2(i,:)=global_4_r2(i,:)+[3 2];
end

for i=151:1:200
    global_4_r1(i,:)=global_4_r1(i,:)+[2 3];
    global_4_r2(i,:)=global_4_r2(i,:)+[3 3];
end
    
figure
plot(global_4_r1(:,1),global_4_r1(:,2),'+',global_4_r2(:,1),global_4_r2(:,2),'o');

% Классификация для распределения Гаусса
r1 = [global_1_r1; global_1_r2; global_1_r3];
mesh = [-6:0.05:6];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLC([X1(i,j) X2(i,j)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

%Подсчет ошибоки классификатора
err = 0;
for i=1:1:size(global_1_r1,1)
    res = SimpleMLC([global_1_r1(i,1) global_1_r1(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);

    if(res ~= 1)
        err = err + 1;
    end
end

for i=1:1:size(global_1_r2,1)
    res = SimpleMLC([global_1_r2(i,1) global_1_r2(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);

    if(res ~= 2)
        err = err + 1;
    end
end

for i=1:1:size(global_1_r3,1)
    res = SimpleMLC([global_1_r3(i,1) global_1_r3(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);

    if(res ~= 3)
        err = err + 1;
    end
end 

err/(size(global_1_r1,1) + size(global_1_r2,1) + size(global_1_r3,1)) * 100

% Классификация для распределения Гаусса с различными вероятностями
r1 = [global_1_r1; global_1_r2; global_1_r3];
mesh = [-6:0.05:6];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCDiff([X1(i,j) X2(i,j)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0.1 0.7 0.2]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

% Классификация для распределения Гаусса с матрицей ошибок
mesh = [-6:0.05:6];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCErr([X1(i,j) X2(i,j)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0 0.1 0.99 ; 0.2 0 0.8; 0.3 0.7 0]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

% Классификация для распределения внутри произвольных областей
r2 = [global_2_r1; global_2_r2; global_2_r3];
mesh = [0:0.1:14];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCRand([X1(i,j) X2(i,j)]);
    end
end
figure
hold on
axis([3 13 0 9]);
colormap([1 0 0; 0 1 0; 0 0 1; 1 1 1]);
contourf(X1,X2,Z,[1 2 3 4],'linestyle','none');

% Классификация для смеси Гаусса
r1 = [global_3_r1; global_3_r2; global_3_r3];
mesh = [-5:0.1:20];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCDistr([X1(i,j) X2(i,j)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

% Классификация для смеси Гаусса с различными вероятностями
r1 = [global_3_r1; global_3_r2; global_3_r3];
mesh = [-5:0.1:20];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCDistrDiff([X1(i,j) X2(i,j)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32,[0.7 0.01 0.29]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

% Классификация для смеси Гаусса с матрицей ошибок
mesh = [-5:0.1:20];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCDistrErr([X1(i,j) X2(i,j)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32,[0 1 99 ; 2 0 1; 3 2 0]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

% Классификация для собственного распределения
mesh = [1:0.01:4];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCMy([X1(i,j) X2(i,j)]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 1 1 1]);
contourf(X1,X2,Z,[1 2 3 4],'linestyle','none');

% Классификация для распределения Гаусса методом ближайших соседей
mesh = [-6:0.1:6];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleKNN([X1(i,j) X2(i,j)], global_1_r1,global_1_r2,global_1_r3,1);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

% Классификация для случайного распределения методом ближайших соседей
mesh = [0:0.1:14];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleKNN([X1(i,j) X2(i,j)], global_2_r1,global_2_r2,global_2_r3,1);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 1 0 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

% Классификация для Гауссовой смеси методом ближайших соседей
mesh = [-5:0.2:20];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleKNN([X1(i,j) X2(i,j)], global_3_r1,global_3_r2,global_3_r3,11);
    end
end
figure
hold on
colormap([1 0 0; 0 1 0; 0 0 1]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');

% Классификация для собственного распределения методом ближайших соседей
mesh = [1:0.1:4];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
for j = 1:1:size(X2)
Z(i,j) = SimpleKNNTwo([X1(i,j) X2(i,j)], global_4_r1,global_4_r2,3);
end
end
figure
hold on
colormap([0 0 1; 1 0 0]);
contourf(X1,X2,Z,[1 2],'linestyle','none');

%Машины опорных векторов
% Распределение Гаусса, случай гауссовых ядер
 load fisheriris
 class=zeros(1,600);
 for i=1:1:200
     class(i) = 1;
 end
 for i=201:1:400
     class(i) = 2;
 end
 for i=401:1:600
     class(i) = 3;
 end
 SVMModels = cell(3,1);
 classes = unique(class);
 rng(1);
 
 indx1 = [true(200,1);false(400,1)];
 indx2 = [false(200,1);true(200,1);false(200,1)];
 indx3 = [false(400,1);true(200,1)];
 
 data = [global_1_r1; global_1_r2; global_1_r3];
 for j = 1:numel(classes);
     indx = false(600,1);
     if j == 1
         indx = indx1;
     elseif j == 2
         indx = indx2;
     elseif j == 3
         indx = indx3;
     end
     
     SVMModels{j} = fitcsvm(data,indx,'ClassNames',[false true],'Standardize',true,...
         'KernelFunction','rbf','BoxConstraint',1);
     
     if j == 1
 sv1 = SVMModels{1}.SupportVectors;
     elseif j == 2
 sv2 = SVMModels{2}.SupportVectors;
     elseif j == 3
 sv3 = SVMModels{3}.SupportVectors;
     end
 end
 d = 0.02;
 [x1Grid,x2Grid] = meshgrid(min(data(:,1)):d:max(data(:,1)),...
     min(data(:,2)):d:max(data(:,2)));
 xGrid = [x1Grid(:),x2Grid(:)];
 N = size(xGrid,1);
 Scores = zeros(N,numel(classes));
 
 for j = 1:numel(classes);
     [~,score] = predict(SVMModels{j},xGrid);
     Scores(:,j) = score(:,2);
 end
 
 [~,maxScore] = max(Scores,[],2);
 figure(164);
 h(1:3) = gscatter(xGrid(:,1),xGrid(:,2),maxScore,...
     [0.1 0.5 0.5; 0.5 0.1 0.5; 0.5 0.5 0.1]);
 hold on
 h(4:6) = gscatter(data(:,1),data(:,2),class);
 hold on
 plot(sv1(:,1),sv1(:,2),'ko','MarkerSize',10);
 hold on
 plot(sv2(:,1),sv2(:,2),'ko','MarkerSize',10);
 hold on
 plot(sv3(:,1),sv3(:,2),'ko','MarkerSize',10);
 title('{\bf Iris Classification Regions}');
 axis tight
 hold off