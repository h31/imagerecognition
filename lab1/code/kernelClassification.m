function res = kernelClassification(mode, modeKDE, point, data1, data2, data3,...
                        h1_1, h2_1, h1_2,h2_2, h1_3,h2_3)
    switch(mode)
        case 1
            prob1 = pkde(modeKDE, point,data1, h1_1, h2_1);
            prob2 = pkde(modeKDE, point,data2, h1_2, h2_2);
            prob3 = pkde(modeKDE, point,data3, h1_3, h2_3);
            [maxValue, index] = max([0, prob1, prob2, prob3]);
            res = index;
        case 2
            prob1 = pkde(modeKDE, point,data1, h1_1, h2_1);
            prob2 = pkde(modeKDE, point,data2, h1_2, h2_2);
            prob3 = pkde(modeKDE, point,data3, h1_3, h2_3);
            res = [prob1 prob2 prob3];
    end
end