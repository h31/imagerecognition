%Машины опорных векторов

 class=zeros(1,600);
 for i=1:1:200
     class(i) = 1;
 end
 for i=201:1:400
     class(i) = 2;
 end
 for i=401:1:600
     class(i) = 3;
 end
 classes = unique(class);
 SVMModels = cell(numel(classes),1);
 
 rng(1);
 
 indx1 = [true(200,1);false(400,1)];
 indx2 = [false(200,1);true(200,1);false(200,1)];
 indx3 = [false(400,1);true(200,1)];
 data = [global_1_r1; global_1_r2; global_1_r3];
 
 % Распределение Гаусса, случай гауссовых ядер
 for j = 1:numel(classes);
     switch j
       case 1
         indx = indx1;
       case 2
         indx = indx2;
       otherwise
         indx = indx3;
     end
     
     SVMModels{j} = fitcsvm(data,indx,'ClassNames',[false true],'Standardize',true, 'KernelFunction','rbf','BoxConstraint',1);
     
     switch j
       case 1
        sv1 = SVMModels{1}.SupportVectors;
       case 2
        sv2 = SVMModels{2}.SupportVectors;
     otherwise
        sv3 = SVMModels{3}.SupportVectors;
     end
 end
 d = 0.02;
 mesh = [-6:0.02:6];
 [x1Grid,x2Grid] = meshgrid(mesh,mesh);
 % [x1Grid,x2Grid] = meshgrid(min(data(:,1)):d:max(data(:,1)), min(data(:,2)):d:max(data(:,2)));
 xGrid = [x1Grid(:),x2Grid(:)];
 N = size(xGrid,1);
 Scores = zeros(N,numel(classes));
 
 for j = 1:numel(classes);
     [~,score] = predict(SVMModels{j},xGrid);
     Scores(:,j) = score(:,2);
 end
 
 w = [0.1 0.5 0.5; 0.5 0.1 0.5; 0.5 0.5 0.1];
 [~,maxScore] = max(Scores,[],2);
 figure;
 h(1:3) = gscatter(xGrid(:,1),xGrid(:,2),maxScore, w);
 hold on
 h(4:6) = gscatter(data(:,1),data(:,2),class,'brg');
 hold on
 plot(sv1(:,1),sv1(:,2),'ko','MarkerSize',10);
 hold on
 plot(sv2(:,1),sv2(:,2),'ko','MarkerSize',10);
 hold on
 plot(sv3(:,1),sv3(:,2),'ko','MarkerSize',10);
 axis([-2 5 -4 2]);
 hold off
 
 %Машины опорных векторов для 3 классов, случай полиномиальный
for j = 1:numel(classes);
    switch j
       case 1
         indx = indx1;
       case 2
         indx = indx2;
       otherwise
         indx = indx3;
     end
    SVMModels{j} = fitcsvm(data,indx,'ClassNames',[false true],'Standardize',true, 'KernelFunction','polynomial','BoxConstraint',1);
end
d = 0.02;
[x1Grid,x2Grid] = meshgrid(mesh,mesh);
% [x1Grid,x2Grid] = meshgrid(min(data(:,1)):d:max(data(:,1)), min(data(:,2)):d:max(data(:,2)));
xGrid = [x1Grid(:),x2Grid(:)];
N = size(xGrid,1);
Scores = zeros(N,numel(classes));

for j = 1:numel(classes);
    [~,score] = predict(SVMModels{j},xGrid);
    Scores(:,j) = score(:,2);
end

[~,maxScore] = max(Scores,[],2);
figure;
h(1:3) = gscatter(xGrid(:,1),xGrid(:,2),maxScore,w);
hold on
h(4:6) = gscatter(data(:,1),data(:,2),class,'brg');
axis([-2 5 -4 2]);
hold off