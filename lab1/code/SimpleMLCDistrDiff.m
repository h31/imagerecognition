function classIndex = SimpleMLCDistrDiff( data, mu1_1, sigma1_1,mu1_2, sigma1_2, mu2_1,sigma2_1,mu2_2, sigma2_2,mu3_1,sigma3_1, mu3_2, sigma3_2,apr)
        probabilityClass1_1 = apr(1) * 1/(2*pi*sqrt(det(sigma1_1)))*exp(-((data-mu1_1)*inv(sigma1_1)*(data-mu1_1)')/2);
        probabilityClass1_2 = apr(1) * 1/(2*pi*sqrt(det(sigma1_2)))*exp(-((data-mu1_2)*inv(sigma1_2)*(data-mu1_2)')/2);
        probabilityClass2_1 = apr(2) * 1/(2*pi*sqrt(det(sigma2_1)))*exp(-((data-mu2_1)*inv(sigma2_1)*(data-mu2_1)')/2);
        probabilityClass2_2 = apr(2) * 1/(2*pi*sqrt(det(sigma2_2)))*exp(-((data-mu2_2)*inv(sigma2_2)*(data-mu2_2)')/2);
        probabilityClass3_1 = apr(3) * 1/(2*pi*sqrt(det(sigma3_1)))*exp(-((data-mu3_1)*inv(sigma3_1)*(data-mu3_1)')/2);
        probabilityClass3_2 = apr(3) * 1/(2*pi*sqrt(det(sigma3_2)))*exp(-((data-mu3_2)*inv(sigma3_2)*(data-mu3_2)')/2);
        [probabilityClass1, index] = max([probabilityClass1_1 probabilityClass1_2]);
        [probabilityClass2, index] = max([probabilityClass2_1 probabilityClass2_2]);
        [probabilityClass3, index] = max([probabilityClass3_1 probabilityClass3_2]);
        [maxValue, index] = max([probabilityClass1 probabilityClass2 probabilityClass3]);
        classIndex = index;
end