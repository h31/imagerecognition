function lab1_3_2( mu_1_1_input,mu_1_2_input, sigma_1_1_input,sigma_1_2_input, r  )
hold on
axis([-5 20 -5 20]);

flags1=['b', 'r', ];
flags2=['g', 'y', ];
n=2; size=500;
mu{1}{1}    = mu_1_1_input;
mu{1}{2}    = mu_1_2_input;
sigma{1}{1} = sigma_1_1_input;
sigma{1}{2} = sigma_1_2_input;
%Численный расчет ММП
GMModels = fitgmdist(r,2);
mu{2}{1} = GMModels.mu(1,:);
mu{2}{2} = GMModels.mu(2,:);
sigma{2}{1} = GMModels.Sigma(:,:,1);
sigma{2}{2} = GMModels.Sigma(:,:,2);

%Построение эллипсов
for k = 1:2
  for j = 1:2
    a=1/((2*pi)^(n/2)*sqrt(sigma{k}{1}(1,1)*sigma{k}{1}(2,2)));
    for i = 1:1:3
        c(i)=a*exp(-i^2/2);
    end
    x = linspace(mu{k}{j}(1)-3*sqrt(sigma{k}{j}(1,1)), mu{k}{j}(1)+3*sqrt(sigma{k}{j}(1,1)),size);
    y = linspace(mu{k}{j}(2)-3*sqrt(sigma{k}{j}(2,2)), mu{k}{j}(2)+3*sqrt(sigma{k}{j}(2,2)),size);
    [X1,X2] = meshgrid(x,y);
    Z      = mvnpdf([X1(:) X2(:)],mu{k}{j},[sigma{k}{j}(1,1) sigma{k}{j}(2,2)]);
    Z      = reshape(Z,length(y),length(x));
    if j == 1
      contour(X1,X2,Z,[c(1), c(2), c(3)], flags1(k));
    else
      contour(X1,X2,Z,[c(1), c(2), c(3)], flags2(k));
    end
  end
end

end

