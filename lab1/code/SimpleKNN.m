function index = SimpleKNN( data, arr1, arr2, arr3, counter )
indexArr = zeros(1, size(arr1,1)+size(arr2,1)+size(arr3,1));
templateArr = [arr1; arr2; arr3];

    for i=1:1:size(arr1)
        indexArr(i)=1;
    end
    
    for i=size(arr1)+1:1:size(arr1)+size(arr2)
        indexArr(i)=2;
    end
    
    for i=size(arr1)+size(arr2)+1:1:size(arr1)+size(arr2)+size(arr3)
        indexArr(i)=3;
    end

    firstClass = 0;
    secondClass = 0;
    thridClass = 0;

    for i=1:1:counter
        minIndex = 1;
        for j=1:1:size(templateArr)
%           current = dist(data,templateArr(j,:)');
%             minIndexVal = dist(data,templateArr(minIndex,:)');
            current = mandist(data,templateArr(j,:)');
            minIndexVal = mandist(data,templateArr(minIndex,:)');
            if (minIndexVal > current)
                minIndex = j;
            end
        end
        
        if(indexArr(minIndex) == 1)
            firstClass = firstClass + 1;
        elseif (indexArr(minIndex) == 2)
            secondClass = secondClass + 1;
        else
            thridClass = thridClass + 1;
        end
        
        templateArr(minIndex,1) = 1000;
        templateArr(minIndex,2) = 1000;
    end
    
    [maxValue, index] = max([firstClass secondClass thridClass]);
end

