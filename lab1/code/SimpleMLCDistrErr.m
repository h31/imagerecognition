function classIndex = SimpleMLCDistrErr( data, mu1_1, sigma1_1,mu1_2, sigma1_2, mu2_1,sigma2_1,mu2_2, sigma2_2,mu3_1,sigma3_1, mu3_2, sigma3_2, errMatrix)
        probabilityClass1_1 = 1/(2*pi*sqrt(det(sigma1_1)))*exp(-((data-mu1_1)*inv(sigma1_1)*(data-mu1_1)')/2);
        probabilityClass1_2 = 1/(2*pi*sqrt(det(sigma1_2)))*exp(-((data-mu1_2)*inv(sigma1_2)*(data-mu1_2)')/2);
        probabilityClass2_1 = 1/(2*pi*sqrt(det(sigma2_1)))*exp(-((data-mu2_1)*inv(sigma2_1)*(data-mu2_1)')/2);
        probabilityClass2_2 = 1/(2*pi*sqrt(det(sigma2_2)))*exp(-((data-mu2_2)*inv(sigma2_2)*(data-mu2_2)')/2);
        probabilityClass3_1 = 1/(2*pi*sqrt(det(sigma3_1)))*exp(-((data-mu3_1)*inv(sigma3_1)*(data-mu3_1)')/2);
        probabilityClass3_2 = 1/(2*pi*sqrt(det(sigma3_2)))*exp(-((data-mu3_2)*inv(sigma3_2)*(data-mu3_2)')/2);
        probabilityClass1 = probabilityClass2_1*errMatrix(1,2) + probabilityClass2_2*errMatrix(1,2) + probabilityClass3_1*errMatrix(1,3) + probabilityClass3_2*errMatrix(1,3);
        probabilityClass2 = probabilityClass1_1*errMatrix(2,1) + probabilityClass1_2*errMatrix(2,1) + probabilityClass3_1*errMatrix(2,3) + probabilityClass3_2*errMatrix(2,3);
        probabilityClass3 = probabilityClass1_1*errMatrix(3,1) + probabilityClass1_2*errMatrix(3,1) + probabilityClass2_1*errMatrix(3,2) + probabilityClass2_2*errMatrix(3,2);
        [maxValue, index] = min([probabilityClass1 probabilityClass2 probabilityClass3]);
        classIndex = index;
end