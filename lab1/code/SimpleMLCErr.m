function classIndex = SimpleMLCErr( data, mu1, sigma1, mu2,sigma2,mu3,sigma3, errMatrix)
        probabilityClass1 = 1/(2*pi*sqrt(det(sigma1)))*exp(-((data-mu1)*inv(sigma1)*(data-mu1)')/2);
        probabilityClass2 = 1/(2*pi*sqrt(det(sigma2)))*exp(-((data-mu2)*inv(sigma2)*(data-mu2)')/2);
        probabilityClass3 = 1/(2*pi*sqrt(det(sigma3)))*exp(-((data-mu3)*inv(sigma3)*(data-mu3)')/2);
        class1 = probabilityClass2*errMatrix(1,2) + probabilityClass3*errMatrix(1,3);
        class2 = probabilityClass1*errMatrix(2,1) + probabilityClass3*errMatrix(2,3);
        class3 = probabilityClass1*errMatrix(3,1) + probabilityClass2*errMatrix(3,2);
        [maxValue, index] = min([class1 class2 class3]);
        classIndex = index;
end