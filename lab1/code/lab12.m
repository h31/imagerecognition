close all;

% Классификация для распределения Гаусса
r1 = [global_1_r1; global_1_r2; global_1_r3];
mesh = [-6:0.05:6];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLC([X1(i,j) X2(i,j)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');
plot(global_1_r1(:, 1), global_1_r1(:, 2), 'y*');
plot(global_1_r2(:, 1), global_1_r2(:, 2), 'm*');
plot(global_1_r3(:, 1), global_1_r3(:, 2), 'c*');
axis([-2 5 -4 2]);

%Подсчет ошибки классификатора
err = 0;
for i=1:1:size(global_1_r1,1)
    res = SimpleMLC([global_1_r1(i,1) global_1_r1(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);
    if(res ~= 1)
        err = err + 1;
    end
end
for i=1:1:size(global_1_r2,1)
    res = SimpleMLC([global_1_r2(i,1) global_1_r2(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);
    if(res ~= 2)
        err = err + 1;
    end
end
for i=1:1:size(global_1_r3,1)
    res = SimpleMLC([global_1_r3(i,1) global_1_r3(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3);
    if(res ~= 3)
        err = err + 1;
    end
end 
err/(size(global_1_r1,1) + size(global_1_r2,1) + size(global_1_r3,1)) * 100

% Классификация для распределения Гаусса с различными вероятностями
r1 = [global_1_r1; global_1_r2; global_1_r3];
mesh = [-6:0.05:6];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCDiff([X1(i,j) X2(i,j)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0.1 0.7 0.2]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');
plot(global_1_r1(:, 1), global_1_r1(:, 2), 'y*');
plot(global_1_r2(:, 1), global_1_r2(:, 2), 'm*');
plot(global_1_r3(:, 1), global_1_r3(:, 2), 'c*');
axis([-2 5 -4 2]);

%Подсчет ошибки классификатора
err = 0;
for i=1:1:size(global_1_r1,1)
    res = SimpleMLCDiff([global_1_r1(i,1) global_1_r1(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0.1 0.7 0.2]);
    if(res ~= 1)
        err = err + 1;
    end
end
for i=1:1:size(global_1_r2,1)
    res = SimpleMLCDiff([global_1_r2(i,1) global_1_r2(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0.1 0.7 0.2]);
    if(res ~= 2)
        err = err + 1;
    end
end
for i=1:1:size(global_1_r3,1)
    res = SimpleMLCDiff([global_1_r3(i,1) global_1_r3(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0.1 0.7 0.2]);
    if(res ~= 3)
        err = err + 1;
    end
end 
err/(size(global_1_r1,1) + size(global_1_r2,1) + size(global_1_r3,1)) * 100

% Классификация для распределения Гаусса с матрицей ошибок
mesh = [-6:0.05:6];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCErr([X1(i,j) X2(i,j)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0 1 2 ; 50 0 2; 50 2 0]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');
plot(global_1_r1(:, 1), global_1_r1(:, 2), 'y*');
plot(global_1_r2(:, 1), global_1_r2(:, 2), 'm*');
plot(global_1_r3(:, 1), global_1_r3(:, 2), 'c*');
axis([-2 5 -4 2]);
%Подсчет ошибки классификатора
err = 0;
for i=1:1:size(global_1_r1,1)
    res = SimpleMLCErr([global_1_r1(i,1) global_1_r1(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0 1 2 ; 50 0 2; 50 2 0]);
    if(res ~= 1)
        err = err + 1;
    end
end
for i=1:1:size(global_1_r2,1)
    res = SimpleMLCErr([global_1_r2(i,1) global_1_r2(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0 1 2 ; 50 0 2; 50 2 0]);
    if(res ~= 2)
        err = err + 1;
    end
end
for i=1:1:size(global_1_r3,1)
    res = SimpleMLCErr([global_1_r3(i,1) global_1_r3(i,2)],global_1_mu1,global_1_sigma1,global_1_mu2, global_1_sigma2,global_1_mu3,global_1_sigma3, [0 1 2 ; 50 0 2; 50 2 0]);
    if(res ~= 3)
        err = err + 1;
    end
end 
err/(size(global_1_r1,1) + size(global_1_r2,1) + size(global_1_r3,1)) * 100


% Классификация для распределения внутри произвольных областей
r2 = [global_2_r1; global_2_r2; global_2_r3];
mesh = [0:0.1:14];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCRand([X1(i,j) X2(i,j)]);
    end
end
figure
hold on
axis([2 9 -1 6]);
colormap([1 0 0; 0 1 0; 0 0 1; 1 1 1]);
contourf(X1,X2,Z,[1 2 3 4],'linestyle','none');
hold on;
plot(global_2_r1(:,1),global_2_r1(:,2),'y+');
hold on;
plot(global_2_r2(:,1),global_2_r2(:,2),'mo');
hold on;
plot(global_2_r3(:,1),global_2_r3(:,2),'c*');
%Подсчет ошибки классификатора
err = 0;
for i=1:1:size(global_2_r1,1)
    res = SimpleMLCRand([global_2_r1(i,1) global_2_r1(i,2)]);
    if(res ~= 1)
        err = err + 1;
    end
end
for i=1:1:size(global_2_r2,1)
    res = SimpleMLCRand([global_2_r2(i,1) global_2_r2(i,2)]);
    if(res ~= 2)
        err = err + 1;
    end
end
for i=1:1:size(global_2_r3,1)
  res = SimpleMLCRand([global_2_r3(i,1) global_2_r3(i,2)]);
  if(res ~= 3)
        err = err + 1;
    end
end 
err/(size(global_2_r1,1) + size(global_2_r2,1) + size(global_2_r3,1)) * 100


% Классификация для смеси Гаусса
r1 = [global_3_r1; global_3_r2; global_3_r3];
mesh = [-5:0.1:20];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCDistr([X1(i,j) X2(i,j)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');
plot(global_3_r1(:,1),global_3_r1(:,2),'y+');
plot(global_3_r2(:,1),global_3_r2(:,2),'mo');
plot(global_3_r3(:,1),global_3_r3(:,2),'c*');

%Подсчет ошибки классификатора
err = 0;
for i=1:1:size(global_3_r1,1)
    res = SimpleMLCDistr([global_3_r1(i,1) global_3_r1(i,2)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32);
    if(res ~= 1)
        err = err + 1;
    end
end
for i=1:1:size(global_3_r2,1)
res = SimpleMLCDistr([global_3_r2(i,1) global_3_r2(i,2)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32);
    if(res ~= 2)
        err = err + 1;
    end
end
for i=1:1:size(global_3_r3,1)
 res = SimpleMLCDistr([global_3_r3(i,1) global_3_r3(i,2)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32);
   if(res ~= 3)
        err = err + 1;
   end
end 
err/(size(global_3_r1,1) + size(global_3_r2,1) + size(global_3_r3,1)) * 100

% Классификация для смеси Гаусса с различными вероятностями
r1 = [global_3_r1; global_3_r2; global_3_r3];
mesh = [-5:0.1:20];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCDistrDiff([X1(i,j) X2(i,j)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32,[0.7 0.01 0.29]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');
plot(global_3_r1(:,1),global_3_r1(:,2),'y+');
plot(global_3_r2(:,1),global_3_r2(:,2),'mo');
plot(global_3_r3(:,1),global_3_r3(:,2),'c*');

%Подсчет ошибки классификатора
err = 0;
for i=1:1:size(global_3_r1,1)
    res = SimpleMLCDistrDiff([global_3_r1(i,1) global_3_r1(i,2)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32,[0.7 0.01 0.29]);
    if(res ~= 1)
        err = err + 1;
    end
end
for i=1:1:size(global_3_r2,1)
res = SimpleMLCDistrDiff([global_3_r2(i,1) global_3_r2(i,2)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32,[0.7 0.01 0.29]);
    if(res ~= 2)
        err = err + 1;
    end
end
for i=1:1:size(global_3_r3,1)
 res = SimpleMLCDistrDiff([global_3_r3(i,1) global_3_r3(i,2)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32,[0.7 0.01 0.29]);
   if(res ~= 3)
        err = err + 1;
   end
end 
err/(size(global_3_r1,1) + size(global_3_r2,1) + size(global_3_r3,1)) * 100

% Классификация для смеси Гаусса с матрицей ошибок
mesh = [-5:0.1:20];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCDistrErr([X1(i,j) X2(i,j)],global_3_mu1_1,global_3_sigma11,global_3_mu1_2,global_3_sigma12,global_3_mu2_1, global_3_sigma21,global_3_mu2_2, global_3_sigma22,global_3_mu3_1,global_3_sigma31,global_3_mu3_2,global_3_sigma32,[0 1 2 ; 50 0 1; 50 2 0]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 0 1 0]);
contourf(X1,X2,Z,[1 2 3],'linestyle','none');
plot(global_3_r1(:,1),global_3_r1(:,2),'y+');
plot(global_3_r2(:,1),global_3_r2(:,2),'mo');
plot(global_3_r3(:,1),global_3_r3(:,2),'c*');

% Классификация для собственного распределения
mesh = [1:0.01:4];
[X1,X2] = meshgrid(mesh,mesh);
Z=zeros(size(X1));
for i=1:1:size(X1)
    for j = 1:1:size(X2)
        Z(i,j) = SimpleMLCMy([X1(i,j) X2(i,j)]);
    end
end
figure
hold on
colormap([0 0 1; 1 0 0; 1 1 1]);
contourf(X1,X2,Z,[1 2 3 4],'linestyle','none');
plot(global_4_r1(:,1),global_4_r1(:,2),'y+',global_4_r2(:,1),global_4_r2(:,2),'co');