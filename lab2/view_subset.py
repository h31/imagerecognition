#!/usr/bin/env python2
# This script is useful to check if the CASIA HWDB1.1 subset was created correctly
import random
import scipy
import sys
from pprint import pprint

import h5py
import itertools
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
from sklearn.feature_selection import f_regression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, _cov
from sklearn.feature_selection import RFE
from sklearn.feature_selection import SelectKBest
from mpl_toolkits.mplot3d import Axes3D
from sklearn.linear_model import LinearRegression

from sklearn.naive_bayes import GaussianNB

from sklearn.metrics.pairwise import euclidean_distances

from sklearn import cross_validation, svm
from sklearn.decomposition import PCA
# from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from collections import defaultdict
from sklearn.neighbors import KNeighborsClassifier, KNeighborsRegressor


def plot_all(X, y, letters):
    # we create an instance of SVM and fit out data. We do not scale our
    # data since we want to plot the support vectors
    C = 1.0  # SVM regularization parameter
    svc = svm.SVC(kernel='rbf').fit(X, y)
    gnb = GaussianNB().fit(X, y)
    knn = KNeighborsClassifier(5).fit(X, y)

    # create a mesh to plot in
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    h = (x_max-x_min)/200  # step size in the mesh
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

    # title for the plots
    titles = ['SVM',
              'Naive Bayes',
              'KNN']

    for i, clf in enumerate((svc, gnb, knn)):
        # Plot the decision boundary. For that, we will assign a color to each
        # point in the mesh [x_min, m_max]x[y_min, y_max].
        plt.subplot(2, 2, i + 1)
        plt.subplots_adjust(wspace=0.4, hspace=0.4)

        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        plt.contourf(xx, yy, Z, cmap=plt.cm.Paired, alpha=0.8)

        # Plot also the training points
        # plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Paired, marker="+*o")
        markers = "+*o"
        col = ['w', 'k', 'm']
        for x in range(letters):
            plt.scatter(X[np.where(x == y), 0], X[np.where(x == y), 1], c=col[x], marker=markers[x])
        plt.xlabel('Feature 1')
        plt.ylabel('Feature 2')
        plt.xlim(xx.min(), xx.max())
        plt.ylim(yy.min(), yy.max())
        plt.xticks(())
        plt.yticks(())
        plt.title(titles[i])

    plt.show()


def select_features(X, y):
    knn = KNeighborsRegressor(5)
    # create the RFE model and select 3 attributes
    rfe = RFE(knn, 3)
    rfe = rfe.fit(X, y)
    # summarize the selection of the attributes
    print(rfe.support_)
    print(rfe.ranking_)


def class_difference(letters, pictures, y):
    distance = np.zeros((letters, letters))

    # for class1 in range(letters):
    #     for class2 in range(letters):
    #         pict1 = pictures[np.where(y == class1)]
    #         pict2 = pictures[np.where(y == class2)]
    #         dist = euclidean_distances(pict1, pict2)
    #         dist_sum = dist.sum()
    #         distance[class1, class2] = dist_sum
    #
    # print(distance)
    # plt.matshow(distance, cmap=cm.Greys)
    # plt.colorbar()
    # plt.show()

    size = 4096
    distance = np.zeros((size, size))

    # for class1 in range(letters):
    #     for class2 in range(letters):
    #         pict1 = pictures[np.where(y == class1)]
    #         pict2 = pictures[np.where(y == class2)]
    #         dist_sum = np.zeros(size)
    #         for feature in range(size):
    #             value1 = pict1[:, feature].reshape((60, 1))
    #             value2 = pict2[:, feature].reshape((60, 1))
    #             dist = euclidean_distances(value1, value2)
    #             dist_sum[feature] = dist.sum()
    #         print("Class 1: {}, class 2: {}".format(class1, class2))
    #         plt.matshow(dist_sum.reshape(64, 64), cmap=cm.Greys)
    #         plt.show()

    dist_sum = np.zeros(size)
    for feature in range(size):
        pict1 = pictures[:, feature]
        pict2 = pictures[:, feature]

        for class1 in range(letters):
            for class2 in range(letters):
                value1 = pict1[np.where(y == class1)].reshape((60, 1))
                value2 = pict2[np.where(y == class2)].reshape((60, 1))

                dist = euclidean_distances(value1, value2)
                dist_sum[feature] += dist.sum()
    plt.matshow(dist_sum.reshape(64, 64), cmap=cm.Greys)
    plt.show()


def cross_valid(cur_x, cur_y, useSVM=True):
    gnb = GaussianNB()
    y_pred = gnb.fit(cur_x, cur_y).predict(cur_x)
    plt.matshow(y_pred.reshape(-1, 30), cmap=cm.Greys)
    plt.colorbar(ticks=range(8))
    plt.show()
    # print("NB res: {}".format(y_pred.reshape(-1, 60)))

    scores = cross_validation.cross_val_score(gnb, cur_x, cur_y, cv=5, n_jobs=4)
    print("NB: %0.2f%% std %0.2f%%" % (scores.mean()*100, scores.std()*100 * 2))
    knn = KNeighborsClassifier(5)
    scores = cross_validation.cross_val_score(knn, cur_x, cur_y, cv=5, n_jobs=4)
    print("KNN: %0.2f%% std %0.2f%%" % (scores.mean()*100, scores.std()*100 * 2))

    if useSVM:
        # C = 1.0  # SVM regularization parameter
        linear_svm = svm.LinearSVC()
        scores = cross_validation.cross_val_score(linear_svm, cur_x, cur_y, cv=5)
        print("SVM: %0.2f%% std %0.2f%%" % (scores.mean()*100, scores.std()*100 * 2))

def main():
    subset_filepath = "HWDB1.1subset.hdf5"

    with h5py.File(subset_filepath, 'r') as f:
        pictures = []
        y = []
        labels = defaultdict(list)

        dset = 'tst'
        print len(f[dset+'/x'])
        for i, arr in enumerate(f[dset+'/y']):
            label = np.where(arr == 1)[0][0]
            labels[label].append(i)

        letters = 3

        for x in range(letters):
            for i in labels[x]:
                print x, i
                bitmap = f[dset + '/x'][i]
                # if x == 2:
                #     plt.imshow(np.squeeze(bitmap, axis=0), cmap=cm.Greys_r)
                #     plt.show()
                pictures.append(bitmap.flatten())
                y.append(x)

        y = np.array(y)
        pictures = np.array(pictures)

        # all_classes = np.zeros((letters, 60, 4096))
        # for x in range(letters):
        #     all_classes[x] = pictures[np.where(y == x)]
        # scipy.io.savemat('out.mat', {"classes2": all_classes}, appendmat=False)

        # class_difference(letters, pictures, y)

        # plt.matshow(y.reshape(-1, 30), cmap=cm.Greys)
        # plt.colorbar(ticks=range(8))
        # plt.show()

        print("Cross valid source")
        cross_valid(pictures, y)

        # for i, item in enumerate(y):
        #     D[item].append(i)
        # D = {k: v for k, v in D.items() if len(v) > 1}
        # for k, v in D.iteritems():
        #     for i in v:
        #         print k, i
        #         plt.imshow(np.squeeze(f[dset+'/x'][i], axis=0), cmap=cm.Greys_r)
        #         plt.show()

        X = np.vstack(pictures)
        pca = PCA(n_components=10)
        X_r = pca.fit(X).transform(X)
        print pca.explained_variance_ratio_
        # plt.matshow(pca.components_[:, 0:2], cmap=cm.Greys)
        # plt.show()

        # selector = SelectKBest(f_regression, k=10)
        # selector.fit(X, y)
        # print "Forward:"
        # print selector.get_support(True)
        # cross_valid(selector.transform(X), y)


        # # Create the RFE object and rank each pixel
        # rfe = RFE(estimator=svm.SVC(kernel='linear'), n_features_to_select=10, step=64)
        # rfe.fit(X, y)
        # ranking = rfe.ranking_.reshape(64, 64)
        #
        # # Plot pixel ranking
        # plt.matshow(ranking)
        # plt.colorbar()
        # plt.title("Ranking of pixels with RFE")
        # plt.show()
        #
        # cross_valid(rfe.transform(X), y)
        #
        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # for i in range(4):
        #     ax.plot([0, pca.components_[0, i]], [0, pca.components_[1, i]], zs=[0, pca.components_[2, i]])
        # plt.show()

        print("Cross valid PCA")
        cross_valid(X_r, y)

        # print("Plot PCA")
        # plot_all(X_r, y, letters)

        lda = LinearDiscriminantAnalysis(n_components=10)
        pca_helper = PCA(n_components=30)
        X_helper = pca_helper.fit(X).transform(X)
        X_r2 = lda.fit(X_helper, y).transform(X_helper)

        # plt.matshow(_cov(X), cmap=cm.Greys)
        # plt.show()

        print("Cross valid LDA")
        cross_valid(X_r2, y)

        # print("Plot LDA")
        # plot_all(X_r2, y, letters)

        # # Percentage of variance explained for each components
        # print('explained variance ratio (first two components): %s'
        #       % str(pca.explained_variance_ratio_))
        #
        # plt.figure()
        # for c, i, target_name in zip("rgbcmk", range(letters), range(letters)):
        #     plt.scatter(X_r[y == i, 0], X_r[y == i, 1], c=c, label=target_name)
        # plt.legend()
        # plt.title('PCA')
        #
        # plt.figure()
        # for c, i, target_name in zip("rgbcmk", range(letters), range(letters)):
        #     plt.scatter(X_r2[y == i, 0], X_r2[y == i, 1], c=c, label=target_name)
        # plt.legend()
        # plt.title('LDA')
        #
        # plt.show()

if __name__ == "__main__":
    main()